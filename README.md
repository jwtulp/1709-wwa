# 1709 World Water Atlas

## Resources

### Used

http://www.diva-gis.org/gdata
A few countries are missing

To download the files, run this command from the `prep/download` folder:
`wget -i url_list.txt --limit-rate=500k -w 10 -nc`

### Others

http://www.hydrosheds.org/
https://hydrosheds.cr.usgs.gov/datadownload.php?reqdata=30rivs
http://gaia.geosci.unc.edu/rivers/

