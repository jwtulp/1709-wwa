const fs = require('fs');

fs.readFile('./static/data/random_points_BWS_s.json', (err, data) => {
  if (err) throw err;

  const json = JSON.parse(data);
  const result = json.features
    .filter(d => d.properties.NUMPOINTS > 0)
    .map(d => ({
      lon: (d.properties.left + d.properties.right) / 2,
      lat: (d.properties.top + d.properties.bottom) / 2,
      val: d.properties.NUMPOINTS,
    }));

  fs.writeFile('./static/data/bws.json', JSON.stringify(result), (err) => {
    if (err) throw err;
  });
});
