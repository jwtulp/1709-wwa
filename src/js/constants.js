import * as THREE from 'three';

export const GLOBE_RADIUS = 5;
export const INIT_ANGLE = Math.PI / 2;
export const INIT_AXIS = new THREE.Vector3(1, 0, 0);
