import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const DEBUG = process.env.NODE_ENV !== 'production';
const PROD = process.env.NODE_ENV === 'production';

const entryDevelopment = [
  'webpack-dev-server/client?/', // Inlines auto-refresh code
  'webpack/hot/dev-server', // Hot Module Replacement
];

module.exports = {
  context: path.join(__dirname, 'src'),
  // devtool: DEBUG ? "inline-sourcemap" : null,
  entry: {
    vendor: [
      'd3-array',
      'd3-color',
      'd3-jetpack',
      'd3-scale',
      'd3-scale-chromatic',
      'd3-selection',
      'ramda',
      'three',
    ],
    app: ['./js/index.js'].concat(PROD ? [] : entryDevelopment),
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [path.resolve(__dirname, 'node_modules')],
    alias: {
      'three-examples': path.join(__dirname, './node_modules/three/examples/js'),
    },
    // modules: [path.resolve('./js/components')],
  },
  devtool: 'eval-source-map',
  // devtool: 'eval-source-map',
  // devtool: PROD ? undefined : "#eval-source-map",
  devServer: {
    contentBase: './static/',
    hot: true,
    host: process.env.HOST,
    port: process.env.PORT,
    watchContentBase: true,
  },
  module: {
    rules: [
      {
        test: /three\/examples\/js/,
        use: 'imports-loader?THREE=three',
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        include: path.join(__dirname, 'src'),
        query: {
          presets: ['es2015', 'stage-0'],
          plugins: ['transform-class-properties', 'transform-decorators-legacy'],
        },
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader?importLoaders=1',
          'postcss-loader',
        ],
      },
      {
        test: /\.png$/,
        use: ['file-loader?name=img/[hash].[ext]'],
      },
      {
        test: /\.glsl$/,
        loader: 'webpack-glsl-loader',
      },
    ],
  },
  output: {
    path: `${__dirname}/dist/`,
    filename: PROD ? 'js/[name].min.js' : 'js/[name].js',
    // filename: 'bundle.min.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'debug'),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
    }),
    new HtmlWebpackPlugin({
      title: 'TEST',
      template: './../static/index.html',
    }),
  ].concat(PROD ? [
    new ExtractTextPlugin('style.css', { allChunks: true }),
  ] : [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    // new BundleAnalyzerPlugin({
    //   analyzerMode: 'disabled',
    // }),
  ]),
};
